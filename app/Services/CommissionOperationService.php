<?php

namespace App\Services;

use App\Helpers\Enum\OperationTypeEnum;
use App\Helpers\Enum\UserTypeEnum;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CommissionOperationService implements CommissionCalculationInterface
{
    private $userType;
    private $operationAmount;
    private $currency;
    private $depositDate;
    private $weeksInfo;
    private $operationType;
    private $currencyService;

    public function __construct($userType, $operationType, $operationAmt, $currency, $depositDate,$weeksInfo)
    {
        $this->weeksInfo      = $weeksInfo;
        $this->setUserType($userType);
        $this->setOperationType($operationType);
        $this->setOperationAmount($operationAmt);
        $this->setCurrency($currency);
        $this->setdepositDate($depositDate);
        $this->currencyService = new CommissionCurrencyService($currency,$operationAmt);
    }

    public function init(): array
    {
        try {
            switch ($this->getOperationType()) {
                case OperationTypeEnum::DEPOSIT:
                    $result = $this->depositRole();
                    break;

                case OperationTypeEnum::WITHDRAW:
                    $result = $this->withDrawRole();
                    break;

                default:
                    $result = 0;
                    break;
            }

            $seconds    = config('misc.cache.currencies');
            Cache::forget('weekly_commission_free');
            Cache::add('weekly_commission_free', json_encode($this->weeksInfo), $seconds);
            $data['commission'] = round_up($result);
            $data['weeksInfo'] = $this->weeksInfo;
        } catch (Exception $exception) {
            $message = ($exception->getMessage()) ?: "Something went wrong";
            Log::error("Commission Calculation Service : " . $exception->getMessage());
            $data['weeksInfo'] = $this->weeksInfo;
            $data['commission'] = $message;
        }
        return $data;
    }

    /**
     * @return float|int
     * @throws Exception
     */
    public function depositRole()
    {
        /*All deposits are charged 0.03% of deposit amount.*/
        $depositChargePer = 0;
        if ($this->getUserType() == UserTypeEnum::PRIVATE) {
            $depositChargePer = config('misc.commission.deposit.usertype.private');
        }
        if ($this->getUserType() == UserTypeEnum::BUSINESS) {
            $depositChargePer = config('misc.commission.deposit.usertype.business');
        }

        $actualAmount = $this->currencyService->actualAmt();

        $actualCommission           = ($actualAmount * $depositChargePer) / 100;

        return $this->currencyService->actualCom($actualCommission);
    }

    /**
     * @return float|int
     * @throws Exception
     */
    public function withDrawRole()
    {
        $actualAmount = 0;
        $withDrawChargePer = 0;
        if ($this->userType == UserTypeEnum::PRIVATE) {
            $withDrawChargePer = config('misc.commission.withdraw.usertype.private');
            $actualAmount         = $this->checkFreeCommission();
        }
        if ($this->userType == UserTypeEnum::BUSINESS) {
            $withDrawChargePer = config('misc.commission.withdraw.usertype.business');
            $actualAmount         = $this->currencyService->actualAmt();
        }

        $actualCommission           = ($actualAmount * $withDrawChargePer) / 100;

        return $this->currencyService->actualCom($actualCommission);
    }


    /**
     * @return float|int|mixed
     * @throws Exception
     */
    public function checkFreeCommission()
    {
        if (count($this->weeksInfo)) {
            foreach ($this->weeksInfo as $key => $weekInfo) {
                $startDate   = $weekInfo['start_date'];
                $endDate     = $weekInfo['end_date'];
                $depositDate = Carbon::parse($this->getDepositDate())->between(Carbon::parse($startDate), Carbon::parse($endDate));
                if ($depositDate) {
                    $actualAmt = $this->currencyService->actualAmt();
                    if ($weekInfo['free_total_amount'] < 1000 && $weekInfo['free_total_count'] < 3) {

                        $chargeFree = 1000 - $weekInfo['free_total_amount'];
                        if ($actualAmt >= $chargeFree) {
                            $freeAmt   = $chargeFree;
                            $remainAmt = $actualAmt - $freeAmt;
                        } else {
                            $freeAmt   = $actualAmt;
                            $remainAmt = 0;
                        }
                        $weekInfo['free_total_amount']              = $weekInfo['free_total_amount'] + $freeAmt;
                        $weekInfo['free_total_count']               = $weekInfo['free_total_count'] + 1;
                        $this->weeksInfo[$key]['free_total_amount'] = $weekInfo['free_total_amount'];
                        $this->weeksInfo[$key]['free_total_count']  = $weekInfo['free_total_count'];
                        $this->weeksInfo[$key]['remaining_amt']     = $remainAmt;
                        return $remainAmt;
                    } else {
                        return $actualAmt;
                    }
                }
            }
        }
        return $this->createNewWeek();
    }

    /**
     * @return float|int
     * @throws Exception
     */
    public function createNewWeek()
    {
        $startDate              = Carbon::parse($this->getDepositDate())->startOfWeek()->toDateString();
        $endDate                = Carbon::parse($this->getDepositDate())->endOfWeek()->toDateString();
        $weekData['week_no']    = $startDate . '_' . $endDate;
        $weekData['start_date'] = $startDate;
        $weekData['end_date']   = $endDate;
        $actualAmt              = $this->currencyService->actualAmt();
        if ($actualAmt >= 1000) {
            $freeAmt   = 1000;
            $remainAmt = $actualAmt - $freeAmt;
        } else {
            $freeAmt   = $actualAmt;
            $remainAmt = 0;
        }
        $weekData['free_total_amount'] = $freeAmt;
        $weekData['free_total_count']  = 1;
        $weekData['remaining_amt']     = $remainAmt;
        $this->weeksInfo[]             = $weekData;
        return $remainAmt;
    }


    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * @param mixed $userType
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
    }

    /**
     * @return mixed
     */
    public function getOperationType()
    {
        return $this->operationType;
    }

    /**
     * @param mixed $operationType
     */
    public function setOperationType($operationType)
    {
        $this->operationType = $operationType;
    }

    /**
     * @return mixed
     */
    public function getOperationAmount()
    {
        return $this->operationAmount;
    }

    /**
     * @param mixed $operationAmount
     */
    public function setOperationAmount($operationAmount)
    {
        $this->operationAmount = $operationAmount;
    }

    /**
     * @return mixed
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param $currency
     * @return void
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @return mixed
     */
    public function getDepositDate()
    {
        return $this->depositDate;
    }

    /**
     * @param $depositDate
     * @return void
     */
    public function setDepositDate($depositDate)
    {
        $this->depositDate = $depositDate;
    }




}
