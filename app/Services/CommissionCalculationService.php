<?php

namespace App\Services;

use App\Helpers\Enum\OperationTypeEnum;
use App\Helpers\Enum\UserTypeEnum;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class CommissionCalculationService
{
    private $weeksInfo;
    private $currencyConverter;

    public function __construct()
    {
        $this->weeksInfo      = [];
    }

    /**
     * @param $userType
     * @param $operationType
     * @param $operationAmt
     * @param $currency
     * @param $depositDate
     * @return float|string
     */
    public function calculateCommission($userType, $operationType, $operationAmt, $currency, $depositDate)
    {
        try {
            if (!in_array($operationType, [OperationTypeEnum::DEPOSIT, OperationTypeEnum::WITHDRAW])) {
                throw new Exception("Invalid Operation Type");
            }
            if (!in_array($userType, [UserTypeEnum::BUSINESS, UserTypeEnum::PRIVATE])) {
                throw new Exception("Invalid User Type");
            }
            $this->currencyConverter = new CommissionOperationService($userType, $operationType, $operationAmt, $currency, $depositDate, $this->weeksInfo);
            $result = $this->currencyConverter->init();
            $commission = $result['commission'];
            $this->weeksInfo = $result['weeksInfo'];
            Cache::forget('weekly_commission_free');
            $seconds    = config('misc.cache.currencies');
            Cache::forget('weekly_commission_free');
            Cache::add('weekly_commission_free', json_encode($this->weeksInfo), $seconds);
            return round_up($commission);
        } catch (Exception $exception) {
            $message = ($exception->getMessage()) ?: "Something went wrong";
            Log::error("Commission Calculation Service : " . $exception->getMessage());
            return $message;
        }
    }
}
