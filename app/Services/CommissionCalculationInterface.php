<?php
namespace App\Services;


interface CommissionCalculationInterface
{

    public function getUserType();

    public function setUserType($userType);

    public function getOperationType();

    public function setOperationType($operationType);

    public function getOperationAmount();

    public function setOperationAmount($operationAmount);

    public function getCurrency();

    public function setCurrency($currency);

    public function getDepositDate();

    public function setDepositDate($depositDate);


}
