<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Cache;

class CommissionCurrencyService
{
    private $currencyApiUrl;
    private $currency;
    private $depositAmount;

    public function __construct($currency, $depositAmount)
    {
        $this->currencyApiUrl = "https://developers.paysera.com/tasks/api/currency-exchange-rates";
        $this->currency = $currency;
        $this->depositAmount = $depositAmount;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getCurrencyRate()
    {
        $cacheCurrencies = Cache::get('currencies');

        if ($cacheCurrencies) {
            $currencies = json_decode($cacheCurrencies, true);
        } else {
            $resultData = callToApi($this->currencyApiUrl, [], [], 'get');
            $result     = json_decode($resultData, true);
            if (isset($result['rates']) && is_array($result['rates'])) {
                $currencies = $result['rates'];
                $seconds    = config('misc.cache.currencies');
                Cache::add('currencies', json_encode($currencies), $seconds);
            } else {
                throw new \Exception("Currency data not found");
            }
        }

        return $currencies[$this->currency];
    }


    /**
     * @return float|int
     * @throws Exception
     */
    public function actualAmt()
    {
        return ($this->depositAmount / $this->getCurrencyRate());
    }

    /**
     * @param $commissionAmt
     * @return float|int
     * @throws Exception
     */
    public function actualCom($commissionAmt)
    {
        return ($commissionAmt * $this->getCurrencyRate());
    }
}
