<?php

namespace App\Http\Controllers\Operation;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommissionRequest;
use App\Services\CommissionCalculationService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class FileUploadController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $commissionData = Cache::get('commissionData');
        if ($commissionData) {
            $items = json_decode($commissionData, true);
        } else {
            $items = [];
        }
        $data['items'] = $items;
        $weekly_commission_free = Cache::get('weekly_commission_free');
        $currencies = Cache::get('currencies');
        $data['weekly_commission_free'] = ($weekly_commission_free) ? json_decode($weekly_commission_free, true) : [];
        $data['currencies'] = ($currencies) ? json_decode($currencies,true) : [];
        $data['i'] = 1;
        return view('operation.commission.index', $data);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View|void
     */
    public function create(Request $request)
    {
        try {
            return view('operation.commission.create');
        } catch (\Exception $exception) {
            \Log::error("File Upload Error: " . $exception->getMessage());
        }
    }


    /**
     * @param CommissionRequest $request
     * @return RedirectResponse
     */
    public function store(CommissionRequest $request): \Illuminate\Http\RedirectResponse
    {
        try {
            $uploadFIle = $request->file('commission_file');
            $fileType = $uploadFIle->getClientMimeType();
            if ($fileType == 'text/csv') {
                $results = csv_to_array($uploadFIle->getRealPath(), ',');
            } else {
                throw new \Exception("Invalid file format   : " . $fileType . " .Valid file format is text/csv. ");
            }
            Cache::forget('commissionData');
            Cache::forget('weekly_commission_free');
            $commissionData = array();

            if (count($results)) {
                $commissionService = new CommissionCalculationService();
                foreach ($results as $result) {
                    if( !isset($result['user_type']) || !isset($result['operation_type']) || !isset($result['operation_amount']) || !isset($result['operation_date']) || !isset($result['currency'])) {
                        throw new \Exception("Missing required data.");
                    }
                    $commissionAmt = $commissionService->calculateCommission($result['user_type'], $result['operation_type'], $result['operation_amount'], $result['currency'], $result['operation_date']);
                    $result['commission'] = $commissionAmt;
                    $commissionData[] = $result;
                }
            }

            $seconds = config('misc.cache.currencies');
            Cache::add('commissionData', json_encode($commissionData), $seconds);
            return redirect()->route('operation.commission.index')->with('success', 'Commission is calculated properly.');
        } catch (\Exception $exception) {
            $message = ($exception->getMessage()) ? $exception->getMessage() : "Something went wrong";
            return redirect()->back()->with('error', $message);
        }
    }
}
