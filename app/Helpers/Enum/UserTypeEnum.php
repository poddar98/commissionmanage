<?php
namespace App\Helpers\Enum;

use App\Helpers\BasicEnum;

class UserTypeEnum extends BasicEnum
{
    const PRIVATE = "private";
    const BUSINESS = "business";
}
