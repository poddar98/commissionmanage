<?php
namespace App\Helpers\Enum;

use App\Helpers\BasicEnum;

class OperationTypeEnum extends BasicEnum
{
    const DEPOSIT = "deposit";
    const WITHDRAW = "withdraw";
}
