<?php

/**/

if (!function_exists('callToApi')) {


    /**
     * @param $url
     * @param $data
     * @param array $header
     * @param string $method
     * @param string $port
     * @return bool|string
     */
    function callToApi($url, $data, $header = [], $method = 'POST')
    {
        try {
            $curl = curl_init();

            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // The default value for this option is 2. It means, it has to have the same name in the certificate as is in the URL you operate against.
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_HEADER, 0);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 15);
            curl_setopt($curl, CURLOPT_TIMEOUT, 20);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            if ($method == 'POST') {
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            }

            $response = curl_exec($curl);
            $err = curl_error($curl);
            $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $curlErrorNo = curl_errno($curl);
            curl_close($curl);

            if ($code == 200 & !($curlErrorNo)) {
                return $response;
            } else {
                $logMessage = "FAILED TO CONNECT WITH EXTERNAL API due to ". $err . " and cURL error code is " . $code . " and response is: " . $response;
                writeToLog($logMessage, 'alert');

                return $response;
            }
        } catch (\Exception $exception) {
            return $exception->getMessage();
        }
    }
}



if (!function_exists('writeToLog')) {
    /**
     * @param $logMessage
     * @param string $logType
     */
    function writeToLog($logMessage, $logType = 'error')
    {
        try {
            $allLogTypes = ['alert', 'critical', 'debug', 'emergency', 'error', 'info'];

            $logType = strtolower($logType);

            if (in_array($logType, $allLogTypes)) {
                \Log::$logType($logMessage);
            }
        } catch (\Exception $exception) {
            //
        }
    }
}


if (! function_exists("csv_to_array")) {
    /**
     * @param $filename
     * @param $delimiter
     * @return array|false
     */
    function csv_to_array($filename='', $delimiter=',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return false;
        }

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false) {
            while (($row = fgetcsv($handle, null, $delimiter)) !== false) {
                if (!$header) {
                    foreach ($row as $r) {
                        $rowData[] = strtolower(str_replace(" ", "_", preg_replace('!\s+!', ' ', $r)));
                    }
                    $header = $rowData;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
        return $data;
    }
};


if (! function_exists('round_up')) {
    /**
     * @param $value
     * @param $precision
     * @return float|string
     */
    function round_up($value, $precision=2)
    {
        $value = (float)$value;
        $precision = (int)$precision;
        if ($precision < 0) {
            $precision = 0;
        }
        $decPointPosition = strpos($value, '.');

        if ($decPointPosition === false) {
            return $value;
        }
        $floorValue = (float)(substr($value, 0, $decPointPosition + $precision + 1));

        $followingDecimals = (int)substr($value, $decPointPosition + $precision + 1);

        if ($followingDecimals) {
            $ceilValue = $floorValue + pow(10, -$precision); // does this give always right result?
        } else {
            $ceilValue = $floorValue;
        }

        return number_format($ceilValue, $precision);
    }
}
