<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('operation.commission.create');
});

Route::group(['namespace' => 'Operation', 'prefix' => 'operation', 'as' => 'operation.'], function () {
    /*
     * These routes need for operation permission
     */
    /*TODO::*/
    Route::get('commissions/generate',[\App\Http\Controllers\Operation\FileUploadController::class,'create'])->name('commission.create');
    Route::get('commissions',[\App\Http\Controllers\Operation\FileUploadController::class,'index'])->name('commission.index');
    Route::post('commissions/generate',[\App\Http\Controllers\Operation\FileUploadController::class,'store'])->name('commission.store');
});
