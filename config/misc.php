<?php
return [
    /*commission rate in percentage*/
    'commission' => [
        'deposit' => [
            'usertype' => [
                'private' => env('DEPOSIT_PRIVATE_COMMISSION_CHARGE_IN_PERCENTAGE','.03'),
                'business' => env('DEPOSIT_BUSINESS_COMMISSION_CHARGE_IN_PERCENTAGE','.03'),
            ]

        ],
        'withdraw' => [
            'usertype' => [
                'private' => env('WITHDRAW_PRIVATE_COMMISSION_CHARGE_IN_PERCENTAGE','.3'),
                'business' => env('WITHDRAW_BUSINESS_COMMISSION_CHARGE_IN_PERCENTAGE','.5'),
            ]
        ]
    ],
    'cache' => [
        'currencies' => env("CURRENCY_CACHE",3600),/*cache need to set in second*/
    ]
];
