### Installation
Follow the instructions below to install the project:

- Clone the repository using `git clone <git-repo-path>`
- Create `.env` by copying the `env.example` file
- need writable permission project `bootstrap/cache` and `storage` path 
- `composer install`
- `php artisan key:generate`
- `php artisan config:cache`
- `composer du`
- then hit your project public url
- Then you will get option to file upload and get commission result instantly
- To test the unit testing please run `php artisan test`
That's it!
