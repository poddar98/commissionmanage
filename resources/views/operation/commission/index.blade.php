@extends('layouts.app')

@section('content')

    <p>
        To upload another commission file please click here.
        <a href="{!! route('operation.commission.create') !!}"> Click Me</a>
    </p>

        <div class="col-md-12">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Operation Date</th>
                    <th scope="col">Number</th>
                    <th scope="col">User Type</th>
                    <th scope="col">Operation Type</th>
                    <th scope="col">Operation Amount</th>
                    <th scope="col">Currency</th>
                    <th scope="col">Commission</th>
                </tr>
                </thead>
                <tbody>
                @if(count($items))
                    @foreach($items as $key=>$item)
                        <tr>
                            <th scope="row">{!! $key+1 !!}</th>
                            <td>{!! $item['operation_date'] ?? '' !!}</td>
                            <td>{!! $item['number'] ?? '' !!}</td>
                            <td>{!! $item['user_type'] ?? '' !!}</td>
                            <td>{!! $item['operation_type'] ?? '' !!}</td>
                            <td>{!! $item['operation_amount'] ?? '' !!}</td>
                            <td>{!! $item['currency'] ?? '' !!}</td>
                            <td>{!! $item['commission'] ?? '' !!}</td>
                        </tr>
                    @endforeach
                @endif

                </tbody>
            </table>
        </div>
    <br>
    <div class="col-md-12">
        <h4>Weekly Commission Free </h4>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">WeekNo</th>
                <th scope="col">StartDate</th>
                <th scope="col">EndDate</th>
                <th scope="col">Commission Amount</th>
                <th scope="col">Commission Count</th>
                <th scope="col">Currency</th>

            </tr>
            </thead>
            <tbody>
            @if(count($weekly_commission_free))
                @foreach($weekly_commission_free as $key=>$commissionFree)
                    <tr>
                        <th scope="row">{!! $key+1 !!}</th>
                        <td>{!! $commissionFree['week_no'] ?? '' !!}</td>
                        <td>{!! $commissionFree['start_date'] .''. \Carbon\Carbon::parse($commissionFree['start_date'])->dayName !!}</td>
                        <td>{!! $commissionFree['end_date'] .''. \Carbon\Carbon::parse($commissionFree['end_date'])->dayName !!}</td>
                        <td>{!! $commissionFree['free_total_amount'] ?? '' !!}</td>
                        <td>{!! $commissionFree['free_total_count'] ?? '' !!}</td>
                        <td>{!!  'EUR' !!}</td>
                    </tr>
                @endforeach
            @endif

            </tbody>
        </table>
    </div>
    <br>
    <div class="col-md-12">
        <h4>Currency Exchange Rate</h4>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Currency Name</th>
                <th scope="col">Rates</th>
                <th scope="col">Base</th>

            </tr>
            </thead>
            <tbody>
            @if(count($currencies))
                @foreach($currencies as $key=>$currency)
                    <tr>
                        <td>{!! $i++; !!}</td>
                        <td>{!! $key ?? '' !!}</td>
                        <td>{!! $currency ?? '' !!}</td>
                        <td>{!!  'EUR' !!}</td>

                    </tr>
                @endforeach
            @endif

            </tbody>
        </table>
    </div>


@endsection
