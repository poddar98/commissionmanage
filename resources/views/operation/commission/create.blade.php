@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">{{ __('Commission File Upload') }}</div>

        <div class="card-body">
            <form method="POST" action="{!! route('operation.commission.store') !!}" enctype="multipart/form-data">
                @csrf

                <div class="row mb-3">
                    <label for="file_name" class="col-md-4 col-form-label text-md-end">{{ __('File Name') }}</label>

                    <div class="col-md-6">
                        <input id="file_name" type="text" class="form-control @error('file_name') is-invalid @enderror" name="file_name" value="{{ old('file_name') }}" required autocomplete="file_name" autofocus>

                        @error('file_name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Uploader Email Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="commission_file" class="col-md-4 col-form-label text-md-end">{{ __('Upload Commission File') }}</label>

                    <div class="col-md-6">
                        <input id="commission_file" type="file" class="form-control @error('commission_file') is-invalid @enderror" name="commission_file"  required  >

                        @error('commission_file')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                        <ul class="small">
                            <li>
                                             <span>
                                               Demo file upload format. To download <a href="{!! asset('demo/demo_commission_upload_file_1.csv') !!}" autofocus> Click Here</a>
                                             </span>
                            </li>
                            <li>File type will be text/csv </li>
                            <li>File format must be as like demo file </li>
                            <li>Operation date in format Y-m-d </li>
                            <li>User's identifier, number</li>
                            <li>User's type, one of private or business</li>
                            <li>Operation type, one of deposit or withdraw</li>
                            <li>Operation amount (for example 2.12 or 3)</li>
                            <li>Operation currency, one of EUR, USD, JPY</li>
                        </ul>
                    </div>
                </div>
                <div class="row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Upload') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
