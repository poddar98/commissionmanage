<?php

namespace Tests\Unit;

use App\Services\CommissionCalculationService;
use Tests\TestCase;

class CalculationCommissionTest extends TestCase
{

    /**
     * @return void
     */
    public function test_private_withdraw_calculation()
    {
        $commissionService = new CommissionCalculationService();
        $result = $commissionService->calculateCommission('private','withdraw','1200.00','EUR','2014-12-31');
        $this->assertEquals(.60, $result);
    }

    /**
     * @return void
     */
    public function test_business_withdraw_calculation()
    {
        $commissionService = new CommissionCalculationService();
        $result = $commissionService->calculateCommission('business','withdraw','300','EUR','2016-01-06');
        $this->assertEquals(1.50, $result);
    }

    public function test_private_deposit_calculation()
    {
        $commissionService = new CommissionCalculationService();
        $result = $commissionService->calculateCommission('private','deposit','200','EUR',' 	2016-01-05');
        $this->assertEquals(0.06, $result);
    }

    public function test_business_deposit_calculation()
    {
        $commissionService = new CommissionCalculationService();
        $result = $commissionService->calculateCommission('business','deposit','200','EUR',' 	2016-01-05');
        $this->assertEquals(0.06, $result);
    }
}
